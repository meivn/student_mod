<?php
/**
 * Grading app renderable.
 *
 * @package    mod_student
 * @since      Moodle 3.1
 * @copyright  2018 My Thuong
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');

class student_contact_form extends moodleform
{

    public function definition()
    {
        $mform = $this->_form;
        $data = $this->_customdata;
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->addElement('hidden', 'studentid');
        $mform->setType('studentid', PARAM_INT);
        $mform->addElement('hidden', 'userid');
        $mform->setType('userid', PARAM_INT);
        $mform->addElement('text', 'name', get_string('contactname', 'student'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addElement('text', 'email', get_string('contactemail', 'student'));
        $mform->setType('email', PARAM_TEXT);
        $mform->addElement('text', 'phone', get_string('contactphone', 'student'));
        $mform->setType('phone', PARAM_INT);
        $this->set_data($data); //chua biet
        // Add standard buttons, common to all modules.
        $this->add_action_buttons();

    }


    /**
     * Export this class data as a flat list for rendering in a template.
     *
     * @param renderer_base $output The current page renderer.
     * @return stdClass - Flat list of exported data.
     */
    public function export_for_template(renderer_base $output)
    {
        global $CFG;
        $export = new stdClass();
        $export->userid = $this->user->id;
        $export->email = $this->user->id;
        $export->name = $this->user->firstname . ' ' . $this->user->lastname;
        $export->phone = $this->user->phone1;
        $export->studentid = $this->studentid;
        return $export;

    }

}
