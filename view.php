<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of student
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_student
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Replace student with the name of your module and remove this line.

require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/locallib.php');

$id = required_param('id', PARAM_INT);
$st = optional_param('st', 0, PARAM_INT);
list ($course, $cm) = get_course_and_cm_from_cmid($id, 'student');

require_login($course, true, $cm);
$student_info = $DB->get_record('student', array('id' => $cm->instance), '*', MUST_EXIST);
$context = context_module::instance($cm->id);

if (!$st = $DB->get_record("student", array("id" => $cm->instance))) {
    print_error('invalidstudentid', 'student');
}

require_capability('mod/student:view', $context);

$student = new student($context, $cm, $course, $st->id);
$urlparams = array('id' => $id,
    'action' => optional_param('action', '', PARAM_ALPHA),
);
$url = new moodle_url('/mod/student/view.php', $urlparams);
$PAGE->set_url($url);

// Get the student class to
// render the page.


$PAGE->set_title(format_string($student_info->name));
$PAGE->set_heading(format_string($course->fullname));

// Output starts here.
echo $OUTPUT->header();

// Replace the following lines with you own code.
echo $student->view(optional_param('action', '', PARAM_ALPHA));

// Finish the page.
echo $OUTPUT->footer();
