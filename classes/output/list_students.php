<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderable that initialises the grading "app".
 *
 * @package    mod_student
 * @copyright  2016 Damyon Wiese
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_student\output;

defined('MOODLE_INTERNAL') || die();

use renderer_base;
use renderable;
use templatable;
use stdClass;

/**
 * Grading app renderable.
 *
 * @package    mod_student
 * @since      Moodle 3.1
 * @copyright  2018 My Thuong
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class list_students implements templatable, renderable
{

    /**
     * @var $userid - The initial user id.
     */
    public $students, $user;

    public function __construct($students = null)
    {
        $this->students = $students;
    }

    /**
     * Export this class data as a flat list for rendering in a template.
     *
     * @param renderer_base $output The current page renderer.
     * @return stdClass - Flat list of exported data.
     */
    public function set_user($value)
    {
        $this->user = $value;
    }

    public function export_for_template(renderer_base $output)
    {
        global $CFG;
        $export = new stdClass();

        $export->students = $this->students;

        $export->user = $this->user;
        return $export;
    }
}
