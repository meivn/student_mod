<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains a renderer for the studentment class
 *
 * @package   mod_student
 * @copyright 2012 NetSpot {@link http://www.netspot.com.au}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/student/locallib.php');

use \mod_student\output;

/**
 * A custom renderer class that extends the plugin_renderer_base and is used by the student module.
 *
 * @package mod_student
 * @copyright 2012 NetSpot {@link http://www.netspot.com.au}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_student_renderer extends plugin_renderer_base
{

    /**
     * Rendering studentment files
     *
     * @param context $context
     * @param int $userid
     * @param string $filearea
     * @param string $component
     * @return string
     */
    /**
     * Defer to template..
     *
     * @param add_contact $app - All the data to render the grading app.
     */
    public function render_add_contact(output\add_contact $app)
    {
        $context = $app->export_for_template($this);
        return $this->render_from_template('mod_student/add_contact', $context);
    }

    public function render_list_students(output\list_students $app)
    {
        $context = $app->export_for_template($this);
        return $this->render_from_template('mod_student/list_students', $context);
    }

    public function render_contact_detail(output\contact_detail $app)
    {
        $context = $app->export_for_template($this);
        return $this->render_from_template('mod_student/contact_detail', $context);
    }

    public function render_edit_contact(output\edit_contact $app)
    {
        $context = $app->export_for_template($this);
        return $this->render_from_template('mod_student/edit_contact', $context);
    }

}

