<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants for module student
 *
 * All the core Moodle functions, neeeded to allow the module to work
 * integrated in Moodle should be placed here.
 *
 * All the student specific functions, needed to implement all the module
 * logic, should go to locallib.php. This will help to save some memory when
 * Moodle is performing actions across all modules.
 *
 * @package    mod_student
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Example constant, you probably want to remove this :-)
 */
define('student_ULTIMATE_ANSWER', 42);

/* Moodle core API */


/**
 * Saves a new instance of the student into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param stdClass $student Submitted data from the form in mod_form.php
 * @param mod_student_mod_form $mform The form instance itself (if needed)
 * @return int The id of the newly inserted student record
 */
function student_add_instance(stdClass $student, mod_student_mod_form $mform = null)
{
    global $DB;

    $student->timecreated = time();

    // You may have to add extra stuff in here.

    $student->id = $DB->insert_record('student', $student);

    return $student->id;
}

/**
 * Updates an instance of the student in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param stdClass $student An object from the form in mod_form.php
 * @param mod_student_mod_form $mform The form instance itself (if needed)
 * @return boolean Success/Fail
 */
function student_update_instance(stdClass $student, mod_student_mod_form $mform = null)
{
    global $DB;

    $student->timemodified = time();
    $student->id = $student->instance;

    // You may have to add extra stuff in here.

    $result = $DB->update_record('student', $student);


    return $result;
}


/**
 * Removes an instance of the student from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function student_delete_instance($id)
{
    global $DB;

    if (!$student = $DB->get_record('student', array('id' => $id))) {
        return false;
    }

    // Delete any dependent records here.

    $DB->delete_records('student', array('id' => $student->id));


    return true;
}


