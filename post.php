<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Edit and save a new student contact
 *
 * @package   mod_student
 * @copyright 2018 My Thuong
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once('../../config.php');
require_once(dirname(__FILE__) . '/lib.php');

$student = optional_param('student', 0, PARAM_INT);
$action = optional_param('action', 0, PARAM_TEXT);
$confirm = optional_param('confirm', 0, PARAM_INT);

$PAGE->set_url('/mod/student/post.php', array(
    'student' => $student,
    'action' => $action,
    'confirm' => $confirm,
));
//these page_params will be passed as hidden variables later in the form.
$page_params = array('student' => $student, 'action' => $action);

$sitecontext = context_system::instance();

// Script is useless unless they're logged in

if (!empty($student)) {      // User is starting a new discussion in a student
    if (!$student = $DB->get_record("student", array("id" => $student))) {
        print_error('invalidstudentid', 'student');
    }
    if (!$course = $DB->get_record("course", array("id" => $student->course))) {
        print_error('invalidcourseid');
    }
    if (!$cm = get_coursemodule_from_instance("student", $student->id, $course->id)) {
        print_error("invalidcoursemodule");
    }
    require_login($course, true, $cm);
    // Retrieve the contexts.
    $modcontext = context_module::instance($cm->id);
    $coursecontext = context_course::instance($course->id);


//
//    echo $OUTPUT->header();

    if (!empty($action)) {
        $params['userid'] = $USER->id;
        $params['studentid'] = $student->id;
        $studentinfo = $DB->get_record('student_contact', $params);// User is editing their own post

        if ($action == 'add') {
            if (empty($studentinfo)) {
                $studentinfo = new stdClass();
                $studentinfo->userid = $USER->id;
                $studentinfo->studentid = $student->id;
                $studentinfo->id = $course->id;
                $backlink = new moodle_url('/mod/student/view.php', array('id' => $modcontext->instanceid));
                $cancellink = $PAGE->url;
                print_add_contact_form($studentinfo, $cancellink, $backlink);
            } else {
                $editlink = new moodle_url('/mod/student/post.php', array('student' => $params['studentid'], 'action' => 'edit'));
                redirect($editlink);
            }
        } else if ($action == 'edit') {
            if (empty($studentinfo)) {
                $addlink = new moodle_url('/mod/student/post.php', array('student' => $params['studentid'], 'action' => 'add'));
                redirect($addlink);
            }
            $backlink = new moodle_url('/mod/student/view.php', array('id' => $modcontext->instanceid));
            $cancellink = $PAGE->url;
            print_edit_contact_form($studentinfo, $cancellink, $backlink);
        }

    }


    echo $OUTPUT->footer();
}
require_once(dirname(__FILE__) . '/contact_form.php');
function print_add_contact_form($studentinfo, $cancellink, $backlink)
{
    global $DB, $PAGE, $CFG, $OUTPUT;
    $action = $CFG->wwwroot . '/mod/student/post.php?student=' . $studentinfo->studentid . '&action=add';
    $mform = new student_contact_form($action, $studentinfo);
    if ($mform->is_cancelled()) {
        redirect($cancellink);
    } else if ($data = $mform->get_data()) {
        $data->timecreated = time();
        $DB->insert_record('student_contact', $data);
        redirect(
            $backlink,
            'Update success',
            null,
            \core\output\notification::NOTIFY_SUCCESS
        );
    } else {
        echo $OUTPUT->header();
        $mform->display();
    }
}

function print_edit_contact_form($studentinfo, $cancellink, $backlink)
{
    global $DB, $PAGE, $CFG, $OUTPUT;
    $action = $CFG->wwwroot . '/mod/student/post.php?student=' . $studentinfo->studentid . '&action=edit';
    $mform = new student_contact_form($action, $studentinfo);
    if ($mform->is_cancelled()) {
        redirect($cancellink);
    } else if ($data = $mform->get_data()) {
        $data->timemodified = time();
        if (!$DB->update_record('student_contact', $data)) {
            print_error('inserterror', 'student_contact');
        }
        redirect(
            $backlink,
            'Update success',
            null,
            \core\output\notification::NOTIFY_SUCCESS
        );
        exit;
    } else {
        echo $OUTPUT->header();
        $mform->display();
    }
}





