@mod @mod_student
@javascript
Feature: Create a new form to add contact for student. If the student have contact info redirect to edit page

  Background:
    Given the following "users" exist:
      | username | firstname | lastname | email                |
      | teacher1 | Tina      | Teacher1 | teacher1@example.com |
      | student1 | Sam1      | 1        | student1@example.com |
      | student2 | Sam2      | Student2 | student2@example.com |
    And the following "courses" exist:
      | fullname | shortname | format |
      | Course 1 | C1        | topics |
    And the following "course enrolments" exist:
      | user     | course | role           |
      | teacher1 | C1     | editingteacher |
      | student1 | C1     | student        |
      | student2 | C1     | student        |
    And I log in as "teacher1"
    And I am on "Course 1" course homepage with editing mode on
    And I add a "Student Info" to section "1" and I fill the form with:
      | student name | Test student     |
      | Description  | student for test |


  Scenario:Show form add contact for student
    When I log out
    And I log in as "student1"
    And I am on "Course 1" course homepage
    And I follow "Test student"
    And I click on "Add Contact Info" "link"
    And I set the following fields to these values:
      | Contact name  | contactname |
      | Contact email | email1      |
      | Contact phone | 1           |
    And I press "Save changes"
    And I should see "contactname"
    And I should see "email1"
    And I should see "1"
    And I log out
    And I log in as "student2"
    And I am on "Course 1" course homepage
    And I follow "Test student"
    And I click on "Add Contact Info" "link"
    And I set the following fields to these values:
      | Contact name  | contactname1 |
      | Contact email | email2       |
      | Contact phone | 2            |
    And I press "Save changes"
    And I should see "contactname1"
    And I should see "email2"
    And I should see "2"
    And I log out
    And I log in as "teacher1"
    And I am on "Course 1" course homepage
    And I follow "Test student"
    And I pause
    And I should see "contactname1"
    And I should see "email2"
    And I should see "2"
    And I should see "contactname"
    And I should see "email1"
    And I should see "1"

