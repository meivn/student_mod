<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * student module data generator class
 *
 * @package mod_student
 * @category test
 * @copyright 2012 Paul Charsley
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_student_generator extends testing_module_generator {
 public $recordid = 0;

    public function create_instance($record = null, array $options = null) {
        $record = (object)(array)$record;

        $defaultsettings = array(
            'alwaysshowdescription'             => 1,
            'submissiondrafts'                  => 1,
            'requiresubmissionstatement'        => 0,
            'sendnotifications'                 => 0,
            'sendstudentnotifications'          => 1,
            'sendlatenotifications'             => 0,
            'duedate'                           => 0,
            'allowsubmissionsfromdate'          => 0,
            'cutoffdate'                        => 0,
            'requireallteammemberssubmit'       => 0,
            'teamsubmissiongroupingid'          => 0,
            'blindmarking'                      => 0,
            'attemptreopenmethod'               => 'none',
            'maxattempts'                       => -1,
            'markingworkflow'                   => 0,

        );

        foreach ($defaultsettings as $name => $value) {
            if (!isset($record->{$name})) {
                $record->{$name} = $value;
            }
        }

        return parent::create_instance($record, (array)$options);
    }
public function create_contact($record){
    global $DB;



    if (!isset($record->studentid)) {
        throw new coding_exception('student id must be present in phpunit_util::create_contact() $record');
    }

    if (!isset($record->userid)) {
        throw new coding_exception('userid must be present in phpunit_util::create_contact() $record');
    }

    if (!isset($record->timecreated)) {
        $record->timecreated = time();
    }

    // Add the contact.
    $this->recordid = $DB->insert_record('student_contact', $record);
    return 5;

}
    public function edit_contact($record){
        global $DB;



        if (!isset($record->id)) {
            throw new coding_exception(' id must be present in phpunit_util::edit_contact() $record');
        }

        if (!isset($record->timemodifed)) {
            $record->timemodifed = time();
        }

        // Add the contact.
        $record->id = $DB->update_record('student_contact', $record);

    }

}
