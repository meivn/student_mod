<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * student search unit tests.
 *
 * @package     mod_student
 * @category    test
 * @copyright   2015 David Monllao {@link http://www.davidmonllao.com}
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->dirroot . '/mod/student/locallib.php');
require_once($CFG->dirroot . '/mod/student/lib.php');

/**
 * Provides the unit tests for student search.
 *
 * @package     mod_student
 * @category    test
 * @copyright   2015 David Monllao {@link http://www.davidmonllao.com}
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_student_add_contact_testcase extends advanced_testcase
{
    protected $students = null;
    protected $teacher1 = null;
    protected $course = null;
    protected $studentmod = null;

    /** @var array $teachers List of DEFAULT_TEACHER_COUNT teachers in the course */

    public function setUp()
    {
        global $DB;

        $this->resetAfterTest();
        $this->course = $this->getDataGenerator()->create_course();
        $record = new stdClass();
        $record->course = $this->course;
        $this->studentmod = $this->getDataGenerator()->create_module('student', $record);
        $this->teacher1 = $this->getDataGenerator()->create_user();
        $this->getDataGenerator()->enrol_user($this->teacher1->id, $this->course->id, 'teacher');

        $userdata = array();
        $userdata['firstname'] = 'student1';
        $userdata['lasttname'] = 'lastname_student1';
// user 2
        $this->students[0] = $this->getDataGenerator()->create_user();

        $this->getDataGenerator()->enrol_user($this->students[0]->id, $this->course->id, 'student');
        $userdata = array();
        $userdata['firstname'] = 'student2';
        $userdata['lasttname'] = 'lastname_student';
        $this->students[1] = $this->getDataGenerator()->create_user();
        $this->getDataGenerator()->enrol_user($this->students[1]->id, $this->course->id, 'student');
    }

    public function test_create_edit_student_contact_for_student()
    {
        $this->setUp();
        global $DB;
        // The student mod.
        $record = new stdClass();
        $record->course = $this->course->id;
        $record->studentid = $this->studentmod->id;
        // Add a contact.
        $record->userid = $this->students[0]->id;
//        add
        $record->id =  self::getDataGenerator()->get_plugin_generator('mod_student')->create_contact($record);
        $recordid =  self::getDataGenerator()->get_plugin_generator('mod_student')->recordid;
        $this->assertEquals(1, $DB->count_records_select('student_contact','studentid = :studentid',
            array('studentid' =>$this->studentmod->id, 'userid' => $this->students[0]->id)));

//        edit
        $record->name = 'thuong';
        self::getDataGenerator()->get_plugin_generator('mod_student')->edit_contact($record);
        $res = $DB->count_records_select('student_contact','id = :id',
        array('id' =>$recordid , 'name' => 'thuong'));

        $this->assertEquals(1, $res);


    }

    public function test_edit_student_contact_for_student()
    {
        $this->setUp();

        global $DB;
        // The student mod.
        $record = new stdClass();
        $record->course = $this->course->id;
        $record->studentid = $this->studentmod->id;

        // Add a contact.
        $record->userid = $this->students[0]->id;
        self::getDataGenerator()->get_plugin_generator('mod_student')->create_contact($record);



    }



}



    