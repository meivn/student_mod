<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Internal library of functions for module student
 *
 * All the student specific functions, needed to implement the module
 * logic, should go here. Never include this file from your lib.php!
 *
 * @package    mod_student
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/*
 * Does something really useful with the passed things
 *
 * @param array $things
 * @return object
 *function student_do_something_useful(array $things) {
 *    return new stdClass();
 *}
 */
require_once(dirname(__FILE__) . '/contact_form.php');

class student
{
    private $course;
    private $studentid;
    private $USER_id;
    private $name;
    private $phone;
    private $email;
    private $USERidlistid;
    private $context, $cm;

    public function __construct($context, $cm, $course, $studentid)
    {
        global $SESSION;
        $this->course = $course;
        $this->context = $context;
        $this->cm = $cm;
        $this->studentid = $studentid;
        // Temporary cache only lives for a single request - used to reduce db lookups.
        $this->cache = array();

        // Extra entropy is required for uniqid() to work on cygwin.
        // $this->useridlistid = clean_param(uniqid('', true), PARAM_ALPHANUM);
    }

    public function set_context(context $context)
    {
        $this->context = $context;
    }

    /**
     * Set the course data.
     *
     * @param stdClass $course The course data
     */
    public function set_course(stdClass $course)
    {
        $this->courseid = $course;
    }

    public function get_course()
    {
        global $DB;

        if ($this->course) {
            return $this->course;
        }
    }

    public function view($action = '', $args = array())
    {
        global $PAGE;
        if (has_capability('mod/student:viewliststudents', $this->context)) {
            $this->show_list_students();
        } else {

            if ($action == '') {
                $this->show_contact_detail();
            } else if ($action == 'add') {
                $this->show_add_contact();
            } else if ($action == 'edit') {
                $this->show_edit_contact();
            }

        }
    }

    public function show_list_students()
    {
        global $DB, $PAGE, $USER;
//        $enrolled = get_enrolled_users($this->context);
//        var_dump($enrolled);
        $sql = "SELECT u.id ,  u.lastname, u.firstname, u.username, u.phone1 as phone, u.email, sc.email as contactemail, sc.phone as contactphone,sc.name as contactname
                  FROM {user} u
                  LEFT JOIN {student_contact} sc ON (sc.userid = u.id)
                  JOIN {user_enrolments} ue ON (ue.userid = u.id)
                  JOIN {enrol} e ON (e.id = ue.enrolid AND e.courseid = :courseid )
                  JOIN {role} ro ON (e.roleid = ro.id)
";
        $params['courseid'] = $this->course->id;
        $students = $DB->get_records_sql($sql, $params);
        $students_res = [];
        foreach ($students as $student) {
            $students_res[] = [
                'id' => $student->id,
                'contactname' => $student->contactname,
                'contactemail' => $student->contactemail,
                'contactphone' => $student->contactphone,
                'firstname' => $student->firstname,
                'lastname' => $student->lastname,
                'phone' => $student->phone,
                'email' => $student->email,

            ];
        }
        $renderable = new \mod_student\output\list_students($students_res);
        $output = $PAGE->get_renderer('mod_student');
        echo $output->render($renderable);

    }


    public function show_contact_detail()
    {
        global $USER, $PAGE, $DB;
        $params['userid'] = $USER->id;
        $params['studentid'] = $this->studentid;
        $student_contact = $DB->get_record('student_contact', $params);
        $renderable = new \mod_student\output\contact_detail($student_contact, $this->course->id, $params['studentid']);
        $output = $PAGE->get_renderer('mod_student');
        echo $output->render($renderable);
    }
}
